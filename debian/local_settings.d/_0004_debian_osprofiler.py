# To enable osprofiler support in horizon, set OPENSTACK_PROFILER config
OPENSTACK_PROFILER = {
    'enabled': False,
    'keys': ['SECRET_KEY'],
    'notifier_connection_string': 'redis://127.0.0.1:6379',
    'receiver_connection_string': 'redis://127.0.0.1:6379'
}
